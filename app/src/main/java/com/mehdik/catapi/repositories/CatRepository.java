package com.mehdik.catapi.repositories;

import com.mehdik.catapi.utils.CatStreams;
import com.mehdik.catapi.models.RandomCat;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;

public class CatRepository {

    private static CatRepository sInstance;
    private RandomCat currentCat;
    private List<RandomCat> currentCatsList;

    public static CatRepository getInstance() {
        if (sInstance == null) {
            sInstance = new CatRepository();
        }
        return sInstance;
    }

    //loading a random cat and keeping it
    public Observable<RandomCat> getRandomCat(){

        return CatStreams
                .getRandomCatStream()
                .flatMap(new Function<RandomCat, ObservableSource<RandomCat>>() {
                    @Override
                    public ObservableSource<RandomCat> apply(RandomCat randomCat) throws Exception {
                        currentCat = randomCat;
                        return Observable.just(currentCat);
                    }
                });
    }

    public void setCurrentCat(RandomCat cat){

        currentCat = cat;
    }

    //loading a random cats list and keeping it
    public Observable<List<RandomCat>> getRandomCatsList(int limit, int page){

        return CatStreams
                .getRandomCatsListStream(limit, page)
                .flatMap(new Function<List<RandomCat>, ObservableSource<List<RandomCat>>>() {
                    @Override
                    public ObservableSource<List<RandomCat>> apply(List<RandomCat> randomCatsList) throws Exception {
                        currentCatsList = randomCatsList;
                        return Observable.just(currentCatsList);
                    }
                });
    }

    public Observable<List<RandomCat>> getCurrentCatsList(){

        return Observable.just(currentCatsList);
    }

    public Observable<RandomCat> getCurrentCat(){

        return Observable.just(currentCat);
    }

}
