package com.mehdik.catapi.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mehdik.catapi.R;
import com.mehdik.catapi.models.RandomCat;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CatAdapter extends RecyclerView.Adapter<CatAdapter.CatViewHolder> {

    public interface OnItemClickListener {
        void selectCat(RandomCat cat);
    }

    private ArrayList<RandomCat> catsList;
    private final OnItemClickListener listener;

    //ajouter un constructeur prenant en entrée une liste
    public CatAdapter(ArrayList<RandomCat> list, OnItemClickListener listener) {
        this.catsList = list;
        this.listener = listener;
    }

    //cette fonction permet de créer les viewHolder
    //et par la même indiquer la vue à inflater (à partir des layout xml)
    @Override
    public CatAdapter.CatViewHolder onCreateViewHolder(ViewGroup viewGroup, int itemType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_cats,viewGroup,false);
        return new CatAdapter.CatViewHolder(view);
    }

    //c'est ici que nous allons remplir notre cellule avec le texte/image de chaque MyObjects
    @Override
    public void onBindViewHolder(CatViewHolder myViewHolder, int position) {

        RandomCat txt = catsList.get(position);
        myViewHolder.bind(txt, listener);
    }

    @Override
    public int getItemCount() {
        return catsList.size();
    }

    public static class CatViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.catIdText)
        TextView catIdText;

        @BindView(R.id.catCardImage)
        ImageView catCardImage;

        @BindView(R.id.infoAvailableImage)
        ImageView infoAvailableImage;

        public CatViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bind(final RandomCat cat, final CatAdapter.OnItemClickListener listener){

            //we display cat image
            Picasso.get()
                    .load(cat.getUrl())
                    //.resize(cat.getWidth(), 300)
                    .fit()
                    .centerCrop()
                    .into(catCardImage);

            catIdText.setText(cat.getId());

            //if more infos are available we display the info icon
            if(cat.getBreeds().size()>0)
                infoAvailableImage.setVisibility(View.VISIBLE);
            else
                infoAvailableImage.setVisibility(View.GONE);

            //if the element is clicked we send the selected cat
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    listener.selectCat(cat);
                }
            });

        }
    }
}