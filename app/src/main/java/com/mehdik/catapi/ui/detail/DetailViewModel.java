package com.mehdik.catapi.ui.detail;

import com.mehdik.catapi.models.RandomCat;
import com.mehdik.catapi.repositories.CatRepository;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.Observable;

public class DetailViewModel extends ViewModel {

    private CatRepository catRepository;

    public DetailViewModel(CatRepository catRepository) {
        this.catRepository = catRepository;
    }


    public Observable<RandomCat> getCurrentCat(){

        return catRepository.getCurrentCat();
    }

    static class Factory extends ViewModelProvider.NewInstanceFactory {

        private CatRepository mCatRepository;

        public Factory() {
        }

        public Factory(@NonNull CatRepository catRepository) {
            mCatRepository = catRepository;
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            //noinspection unchecked
            return (T) new DetailViewModel(mCatRepository);
        }
    }

}
