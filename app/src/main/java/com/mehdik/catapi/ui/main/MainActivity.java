package com.mehdik.catapi.ui.main;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.mehdik.catapi.R;
import androidx.lifecycle.ViewModelProviders;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.mehdik.catapi.models.RandomCat;
import com.mehdik.catapi.repositories.CatRepository;
import com.mehdik.catapi.ui.adapters.CatAdapter;
import com.mehdik.catapi.ui.detail.DetailActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private MainViewModel mViewModel;
    private Disposable disposable;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private static String TAG = "KERMAD";

    @BindView(R.id.catsRecycler)
    RecyclerView catsRecycler;

    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        mViewModel = ViewModelProviders.of(this, new MainViewModel.Factory(CatRepository.getInstance())).get(MainViewModel.class);

        layoutManager = new GridLayoutManager(this, 3);
        catsRecycler.setLayoutManager(layoutManager);

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getRandomCatsList(9, 0); //getting a random list of cats
            }
        });

        //initializing the activity with data
        swipeRefresh.setRefreshing(true);
        getRandomCatsList(9, 0); //getting a random list of cats
    }

    private void getRandomCatsList(int limit, int page) {

        disposable = mViewModel.getRandomCatsList(limit, page)
                .subscribeWith(new DisposableObserver<List<RandomCat>>(){
                    @Override
                    public void onNext(List<RandomCat> randomCatsList) {

                        Log.e(TAG, randomCatsList.toString());

                        mAdapter = new CatAdapter(new ArrayList<RandomCat>(randomCatsList), new CatAdapter.OnItemClickListener() {
                            @Override
                            public void selectCat(RandomCat cat) {

                                //blocking the DetailView if currently refreshing datas
                                if(!swipeRefresh.isRefreshing()) {

                                    Log.e(TAG, cat.toString());

                                    //we save the selected cat
                                    mViewModel.setCurrentCat(cat);

                                    //go to DetailActivity
                                    Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                                    startActivity(intent);
                                }
                            }
                        });

                        catsRecycler.setAdapter(mAdapter);
                        swipeRefresh.setRefreshing(false);
                    }

                    @Override
                    public void onError(Throwable e) {

                        Log.e(TAG, "error cat : "+e.toString());
                        Toast.makeText(MainActivity.this, getString(R.string.cat_error), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {

                        Log.e(TAG, "complete");
                    }
                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        disposeSubscribers();
    }

    private void disposeSubscribers(){

        if(disposable!=null && !disposable.isDisposed())
            disposable.dispose();
    }
}
