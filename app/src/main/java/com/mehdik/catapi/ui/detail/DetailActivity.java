package com.mehdik.catapi.ui.detail;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.mehdik.catapi.R;
import com.mehdik.catapi.models.Breed;
import com.mehdik.catapi.models.RandomCat;
import com.mehdik.catapi.repositories.CatRepository;
import com.mehdik.catapi.ui.main.MainViewModel;
import com.mehdik.catapi.utils.CircleTransform;
import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {

    private DetailViewModel mViewModel;
    private Disposable disposable;

    @BindView(R.id.catImageView)
    ImageView catImageView;

    @BindView(R.id.nameText)
    TextView nameText;

    @BindView(R.id.descriptionText)
    TextView descriptionText;

    @BindView(R.id.weightText)
    TextView weightText;

    @BindView(R.id.originText)
    TextView originText;

    @BindView(R.id.temperamentText)
    TextView temperamentText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ButterKnife.bind(this);

        mViewModel = ViewModelProviders.of(this, new DetailViewModel.Factory(CatRepository.getInstance())).get(DetailViewModel.class);

        //hide actionBar
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        disposable = mViewModel.getCurrentCat()
                .subscribeWith(new DisposableObserver<RandomCat>(){
                    @Override
                    public void onNext(RandomCat randomCat) {

                        displayCatInfo(randomCat);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void displayCatInfo(RandomCat cat){

        //we display cat image
        Picasso.get()
                .load(cat.getUrl())
                .transform(new CircleTransform())
                .fit()
                .centerCrop()
                .into(catImageView);

        //we display basic infos if we have it
        if(cat.getBreeds().size()>0)
        {
            final Breed breed = cat.getBreeds().get(0);

            if(!breed.getName().isEmpty())
                nameText.setText(breed.getName());
            if(!breed.getDescription().isEmpty())
                descriptionText.setText(breed.getDescription());
            if(breed.getWeight()!=null)
                weightText.setText(breed.getWeight().getMetric());
            if(!breed.getOrigin().isEmpty())
                originText.setText(breed.getOrigin());
            if(!breed.getTemperament().isEmpty())
                temperamentText.setText(breed.getTemperament());
        }
    }
}
