package com.mehdik.catapi.ui.main;

import com.mehdik.catapi.models.RandomCat;
import com.mehdik.catapi.repositories.CatRepository;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.Observable;

public class MainViewModel extends ViewModel {

    private CatRepository catRepository;

    public MainViewModel(CatRepository catRepository) {
        this.catRepository = catRepository;
    }

    //loading a random cat
    public Observable<RandomCat> getRandomCat(){

        return catRepository.getRandomCat();
    }

    //loading a random cats list
    public Observable<List<RandomCat>> getRandomCatsList(int limit, int page){

        return catRepository.getRandomCatsList(limit, page);
    }

    public void setCurrentCat(RandomCat cat){

        catRepository.setCurrentCat(cat);
    }

    static class Factory extends ViewModelProvider.NewInstanceFactory {

        private CatRepository mCatRepository;

        public Factory() {
        }

        public Factory(@NonNull CatRepository catRepository) {
            mCatRepository = catRepository;
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            //noinspection unchecked
            return (T) new MainViewModel(mCatRepository);
        }
    }

}
