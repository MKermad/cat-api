package com.mehdik.catapi.utils;

import com.mehdik.catapi.models.RandomCat;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class CatStreams {

    public static Observable<RandomCat> getRandomCatStream(){

        final CatService catService = CatService.retrofit.create(CatService.class);

        return catService.getRandomCat()
                .flatMap(new Function<List<RandomCat>, ObservableSource<RandomCat>>() {
                    @Override
                    public ObservableSource<RandomCat> apply(List<RandomCat> randomCats) throws Exception {
                        return Observable.just(randomCats.get(0));
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .timeout(10, TimeUnit.SECONDS);
    }

    public static Observable<List<RandomCat>> getRandomCatsListStream(int limit, int page){

        final CatService catService = CatService.retrofit.create(CatService.class);

        return catService.getRandomCatsList(limit, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .timeout(10, TimeUnit.SECONDS);
    }
}
