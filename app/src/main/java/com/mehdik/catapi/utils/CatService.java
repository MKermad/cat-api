package com.mehdik.catapi.utils;

import com.mehdik.catapi.models.RandomCat;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface CatService {

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://api.thecatapi.com/v1/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build();

    @Headers("x-api-key: 2dfd1909-0732-4e01-add9-3fa4317f2cee")
    @GET("images/search")
    Observable<List<RandomCat>> getRandomCat();

    @Headers("x-api-key: 2dfd1909-0732-4e01-add9-3fa4317f2cee")
    @GET("images/search")
    Observable<List<RandomCat>> getRandomCatsList(@Query("limit") int limit,
                                                  @Query("page") int page);
}