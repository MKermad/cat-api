package com.mehdik.catapi;

import com.mehdik.catapi.models.RandomCat;
import com.mehdik.catapi.utils.CatStreams;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class CatStreamsTest {
    @Test
    public void getRandomCatStreamTest() {

        //getting the observable to test
        Observable<RandomCat> randomCatObservable = CatStreams.getRandomCatStream();

        TestObserver<RandomCat> testObserver = new TestObserver<>();

        //launching subscription
        randomCatObservable
                .subscribeWith(testObserver)
                .assertNoErrors() //checks no errors
                .assertNoTimeout() //checks no timeout
                .awaitTerminalEvent(); //waiting the end before continue

        //we get the random cat (it returns a list)
        RandomCat randomCat = testObserver.values().get(0);

        //tests datas
        assertNotNull("Random Cat ID is null", randomCat.getId());
        assertNotNull("Random Cat URL is null", randomCat.getUrl());
        assertNotEquals("Random Cat image width equals 0", (int) randomCat.getWidth(), 0);
        assertNotEquals("Random Cat image height equals 0", (int) randomCat.getHeight(), 0);
    }

    @Test
    public void getRandomCatsListStreamTest() {

        //getting the observable to test
        Observable<List<RandomCat>> randomCatsListObservable = CatStreams.getRandomCatsListStream(9, 0);

        TestObserver<List<RandomCat>> testObserver = new TestObserver<>();

        //launching subscription
        randomCatsListObservable
                .subscribeWith(testObserver)
                .assertNoErrors() //checks no errors
                .assertNoTimeout() //checks no timeout
                .awaitTerminalEvent(); //waiting the end before continue

        //we get the random cat
        List<RandomCat> randomCatsList = testObserver.values().get(0);
        RandomCat randomCat = randomCatsList.get(0);

        //tests datas
        assertEquals("Random Cats list has less than 9 elements", randomCatsList.size(), 9);
        assertNotNull("Random Cat ID is null", randomCat.getId());
        assertNotNull("Random Cat URL is null", randomCat.getUrl());
        assertNotEquals("Random Cat image width equals 0", (int) randomCat.getWidth(), 0);
        assertNotEquals("Random Cat image height equals 0", (int) randomCat.getHeight(), 0);
    }
}
