package com.mehdik.catapi;

import com.mehdik.catapi.models.RandomCat;
import com.mehdik.catapi.ui.main.MainViewModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(JUnit4.class)
public class MainViewModelTest {

    @Mock
    private MainViewModel mainViewModel;
    private RandomCat testCat;
    private List<RandomCat> testCatsList;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        testCat = new RandomCat();
        testCat.setId("testCat");
        testCat.setHeight(123);
        testCat.setWidth(321);
        testCat.setUrl("http://www.fakeurl.com");

        testCatsList = new ArrayList<>();
        testCatsList.add(testCat);
        testCatsList.add(new RandomCat());
        testCatsList.add(new RandomCat());

        Mockito.when(mainViewModel.getRandomCat()).thenReturn(Observable.just(testCat));
        Mockito.when(mainViewModel.getRandomCatsList(3, 0)).thenReturn(Observable.just(testCatsList));
    }

    @Test
    public void getRandomCatMainVMTest() {

        //getting the observable to test
        Observable<RandomCat> randomCatObservable = mainViewModel.getRandomCat();

        TestObserver<RandomCat> testObserver = new TestObserver<>();

        //launching subscription
        randomCatObservable
                .subscribeWith(testObserver)
                .assertNoErrors() //checks no errors
                .assertNoTimeout() //checks no timeout
                .awaitTerminalEvent(); //waiting the end before continue

        //we get the random cat (it returns a list)
        RandomCat randomCat = testObserver.values().get(0);

        //tests datas
        assertEquals("Random Cat ID is different", randomCat.getId(), testCat.getId());
        assertEquals("Random Cat URL is different", randomCat.getUrl(), testCat.getUrl());
        assertEquals("Random Cat image width is different", (int) randomCat.getWidth(), (int) testCat.getWidth());
        assertEquals("Random Cat image height is different", (int) randomCat.getHeight(), (int) testCat.getHeight());
    }

    @Test
    public void getRandomCatsListMainVMTest() {

        //getting the observable to test
        Observable<List<RandomCat>> randomCatsListObservable = mainViewModel.getRandomCatsList(3, 0);

        TestObserver<List<RandomCat>> testObserver = new TestObserver<>();

        //launching subscription
        randomCatsListObservable
                .subscribeWith(testObserver)
                .assertNoErrors() //checks no errors
                .assertNoTimeout() //checks no timeout
                .awaitTerminalEvent(); //waiting the end before continue

        //we get the random cat (it returns a list)
        List<RandomCat> randomCatsList = testObserver.values().get(0);

        //tests datas
        assertEquals("Random Cats list size is different", randomCatsList.size(), testCatsList.size());
        assertEquals("Random Cat ID is different", randomCatsList.get(0).getId(), testCatsList.get(0).getId());
        assertEquals("Random Cat URL is different", randomCatsList.get(0).getUrl(), testCatsList.get(0).getUrl());
        assertEquals("Random Cat image is different", (int) randomCatsList.get(0).getWidth(), (int) testCatsList.get(0).getWidth());
        assertEquals("Random Cat image is different", (int) randomCatsList.get(0).getHeight(), (int) testCatsList.get(0).getHeight());
    }
}
