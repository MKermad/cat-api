package com.mehdik.catapi;

import com.mehdik.catapi.models.RandomCat;
import com.mehdik.catapi.ui.detail.DetailViewModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(JUnit4.class)
public class DetailViewModelTest {

    @Mock
    private DetailViewModel detailViewModel;
    private RandomCat testCat;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        testCat = new RandomCat();
        testCat.setId("testCat");
        testCat.setHeight(123);
        testCat.setWidth(321);
        testCat.setUrl("http://www.fakeurl.com");

        Mockito.when(detailViewModel.getCurrentCat()).thenReturn(Observable.just(testCat));
    }

    @Test
    public void getCurrentCatDetailVMTest() {

        //getting the observable to test
        Observable<RandomCat> randomCatObservable = detailViewModel.getCurrentCat();

        TestObserver<RandomCat> testObserver = new TestObserver<>();

        //launching subscription
        randomCatObservable
                .subscribeWith(testObserver)
                .assertNoErrors() //checks no errors
                .assertNoTimeout() //checks no timeout
                .awaitTerminalEvent(); //waiting the end before continue

        //we get the random cat (it returns a list)
        RandomCat randomCat = testObserver.values().get(0);

        //tests datas
        assertEquals("Random Cat ID is different", randomCat.getId(), testCat.getId());
        assertEquals("Random Cat URL is different", randomCat.getUrl(), testCat.getUrl());
        assertEquals("Random Cat image width is different", (int) randomCat.getWidth(), (int) testCat.getWidth());
        assertEquals("Random Cat image height is different", (int) randomCat.getHeight(), (int) testCat.getHeight());
    }
}
